/*
SQLyog Trial v12.4.1 (32 bit)
MySQL - 10.1.19-MariaDB : Database - uyeler2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`uyeler2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `uyeler2`;

/*Table structure for table `alinanders` */

DROP TABLE IF EXISTS `alinanders`;

CREATE TABLE `alinanders` (
  `no` varchar(15) DEFAULT NULL,
  `hoca` varchar(15) DEFAULT NULL,
  `ders` varchar(15) DEFAULT NULL,
  `konu` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `alinanders` */

insert  into `alinanders`(`no`,`hoca`,`ders`,`konu`) values 
('152','?ENOL','tepkime','K?MYA'),
('No','?ENOL','K?MYA','sds');

/*Table structure for table `notlar` */

DROP TABLE IF EXISTS `notlar`;

CREATE TABLE `notlar` (
  `numara` varchar(15) DEFAULT NULL,
  `dersler` varchar(15) DEFAULT NULL,
  `aldigi` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `notlar` */

insert  into `notlar`(`numara`,`dersler`,`aldigi`) values 
('152','Fizik','100'),
('152','Kimya','100'),
('152','Kimya','745');

/*Table structure for table `ogrenci` */

DROP TABLE IF EXISTS `ogrenci`;

CREATE TABLE `ogrenci` (
  `isim` varchar(15) DEFAULT NULL,
  `soyisim` varchar(15) DEFAULT NULL,
  `no` varchar(15) DEFAULT NULL,
  `sifre` varchar(15) DEFAULT NULL,
  `bulum` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ogrenci` */

insert  into `ogrenci`(`isim`,`soyisim`,`no`,`sifre`,`bulum`) values 
('AD','Soyad','152','152','BÖLÜM');

/*Table structure for table `ogretmen` */

DROP TABLE IF EXISTS `ogretmen`;

CREATE TABLE `ogretmen` (
  `isim` varchar(15) DEFAULT NULL,
  `soyisim` varchar(15) DEFAULT NULL,
  `brans` varchar(15) DEFAULT NULL,
  `maas` varchar(15) DEFAULT NULL,
  `kulanici` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ogretmen` */

insert  into `ogretmen`(`isim`,`soyisim`,`brans`,`maas`,`kulanici`) values 
('ali','kaya','fizik','1500','152'),
('ali','kaya','fizik','1500','152');

/*Table structure for table `yetkili` */

DROP TABLE IF EXISTS `yetkili`;

CREATE TABLE `yetkili` (
  `kulanici` varchar(15) DEFAULT NULL,
  `sifre` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `yetkili` */

insert  into `yetkili`(`kulanici`,`sifre`) values 
('a1','123');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
