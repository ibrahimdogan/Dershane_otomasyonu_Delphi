unit Unit7;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  Vcl.Grids, Vcl.DBGrids, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TForm7 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    ComboBox1: TComboBox;
    Edit4: TEdit;
    FDConnection1: TFDConnection;
    FDQuery1: TFDQuery;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

{$R *.dfm}

procedure TForm7.Button1Click(Sender: TObject);
begin
with FDQuery1 do
begin

   // veritaban�na ekleme i�lemler
   if (Edit1.Text <> '') and (Edit2.Text <> '')and(Edit3.Text <> '') and (Edit4.Text <> '') and(ComboBox1.Text <> '') and (Edit4.Text <> '') then
   begin
      close;
  SQL.Text:='insert into  ogretmen(isim,soyisim,brans,maas,kulanici) values(:isim,:soyisim,:brans,:maas,:kulanici)' ;
   ParamByName('isim').Value:=Edit1.Text;
   ParamByName('soyisim').Value:=Edit2.Text;
   ParamByName('maas').Value:=Edit3.Text;
   ParamByName('kulanici').Value:=Edit4.Text;
   ParamByName('brans').Value:=ComboBox1.Text;

   ExecSQL;
   Close;
   SQL.Text:='select * from ogretmen where kulanici='+#39+Edit1.Text+#39;
   ShowMessage('Eklendi');
  Open;
   end
   else
   begin
     ShowMessage('L�tfen Formu Doldurunuz...');
   end;


  end;
end;
procedure TForm7.Button2Click(Sender: TObject);
begin
with FDQuery1 do
begin
if(Edit3.Text <> '') then
begin
  close;
  SQL.Text:='delete  from  ogretmen where kulanici='+#39+Edit4.Text+#39;
  ExecSQL;
   Close;
   SQL.Text:='select * from ogretmen where kulanici='+#39+Edit4.Text+#39;
  Open;
  ShowMessage('Silindi');
 end
 else
 begin
     ShowMessage('��renci No Giriniz...');
 end;
  end;


end;

procedure TForm7.Button3Click(Sender: TObject);
begin
with FDQuery1 do
  begin
     Close;
     SQL.Text:='UPDATE ogretmen set isim="'+Edit1.Text+'",soyisim="'+Edit2.Text+'",brans="'+ComboBox1.Text+'",maas="'+Edit3.Text+'" WHERE kulanici="'+Edit4.Text+'"';
     ExecSQL;
     SQL.Text:='SELECT * FROM ogretmen';
     Open;
     ShowMessage(Edit1.Text +' g�ncellenmi�tir.');
  end;
end;
end.
